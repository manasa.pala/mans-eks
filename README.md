# mans-eks
Give your machine's public ip in the variable "accessing_computer_ip" in variables.tf.

subscribe to an AMI in market place and update filter value under data "aws_ami" in worker_nodes.tf.

Install jq on your machine

After the cluster is provisioned use the following command in terminal to update kubeconfig

aws eks --region <region_name> update-kubeconfig --name <cluster_name>

Run 'kubectl get nodes' to see the list of nodes 